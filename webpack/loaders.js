const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const fs = require('fs');
const lessToJs = require('less-vars-to-js');

const themeVars = path.join(
  __dirname,
  '../src/css/antd-theme-overrides.less',
);
const themeVariables = lessToJs(fs.readFileSync(themeVars, 'utf8'));

const CSSLoader = {
  test: /\.css$/,
  use: ['style-loader', 'css-loader'],
};

const LESSLoader =  {
  test: /\.less$/,
  use: [
    'style-loader',
    { loader: 'css-loader', options: { importLoaders: 1 } },
    {
      loader: 'less-loader',
      options: { javascriptEnabled: true, modifyVars: themeVariables },
    },
  ],
};

const JSLoader = {
  test: /\.(ts|js)x?$/,
  use: {
    loader: 'babel-loader',
  },
  include: [
    path.resolve(__dirname, '../src'),
  ],
  enforce: 'pre',
};


module.exports = {
  JSLoader: JSLoader,
  CSSLoader: CSSLoader,
  LESSLoader: LESSLoader,
};
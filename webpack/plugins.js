const _MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin }  = require('clean-webpack-plugin');
const webpack = require('webpack');

const MiniCssExtractPlugin = new _MiniCssExtractPlugin({
  filename: '[name].css',
  chunkFilename: '[id].css'
});

module.exports = {
  MiniCssExtractPlugin: MiniCssExtractPlugin,
  CleanWebpackPlugin: new CleanWebpackPlugin,
  HotModuleReplacementPlugin: new webpack.HotModuleReplacementPlugin(),
};
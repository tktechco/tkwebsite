module.exports = {
  plugins: {
    'postcss-import': {},
    'postcss-preset-env': {   // does fancy pollyfill for browser css support
      browsers: 'last 2 versions',
    },
    'cssnano': {},  // css minifier. uses lots of postcss
  },
};
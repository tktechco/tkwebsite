const path = require('path');
const nodeExternals = require('webpack-node-externals');
const loaders = require('./loaders');
const plugins = require('./plugins');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const serverConfig = {
  mode: 'development',
  devtool: 'cheap-module-source-map',
  resolve: {
    extensions: ['.ts', '.tsx', '.jsx', '.js', '.json'],
  },
  //devtool: 'source-map',
  target: 'node',
  node: {
    __dirname: false,
  },
  externals: [nodeExternals()],
  entry: {
    'server.js': path.resolve(__dirname, '../src/server.tsx'),
  },
  module: {
    rules: [
      loaders.CSSLoader,
      loaders.JSLoader
    ]
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name]',
    publicPath: '/', // for webpack middleware
  },
  plugins: [
    plugins.MiniCssExtractPlugin,
    plugins.CleanWebpackPlugin,
  ],
}

const clientConfig = {
  mode: 'development',
  devtool: 'cheap-module-source-map',
  resolve: {
    extensions: ['.ts', '.tsx', '.jsx', '.js', '.json'],
  },
  target: 'web',
  // using this entry format, chunk splitting can have dynamic names, server doesnt need it
  entry: [path.join(__dirname, '../src/client.tsx')],
  module: {
    rules: [
      loaders.CSSLoader,
      loaders.JSLoader,
      {
        test: /\.less$/,
        use: [
          {loader: "style-loader"},
          {loader: "css-loader"},
          {
            loader: "less-loader",
            options: {
              javascriptEnabled: true,
            }
          }
        ]
      },
    ]
  },
  output: {
    path: path.resolve(__dirname, '../dist/public'),
    // filename: 'assets/scripts/[name].js'
    filename: 'main.js',
    publicPath: '/', // for webpack middleware
  },
  plugins: [
    plugins.MiniCssExtractPlugin,
    plugins.CleanWebpackPlugin,
    plugins.HotModuleReplacementPlugin,
    new HtmlWebpackPlugin({
      filename: '../dist/public/index.html',
      // template: 'src/front/statics/index.html',  // for code splitting ?
    }),
  ],
  optimization: {
    runtimeChunk: false,
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true,
        },
      },
    },
  },
};

module.exports = [serverConfig, clientConfig];


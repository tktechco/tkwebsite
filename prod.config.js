/*
* Tom Kobyluch
* prod.config.js
*
* TODO:
*
* Notes:
*  - Using nodeexternals means your server side file is tiny, because it does not compile node modules with it
*  - This means it relies on accessing public assets for react libraries
*  - enforce pre was from when I was chaining multiple transforms, with css to js also
* */


const webpack = require('webpack');
const path = require('path');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const TerserPlugin = require('terser-webpack-plugin');

const js = {
  test: /\.(ts|js)x?$/,
  use: ['babel-loader'],
  exclude: /node_modules/,
  enforce: 'pre',
};

const serverConfig = {
  mode: 'production',
  resolve: {
    extensions: ['.ts', '.tsx', '.jsx', '.js', '.json'],
  },
  target: 'node',
  node: {
    __dirname: false,
  },
  externals: [nodeExternals()],
  entry: {
    'server.js': path.resolve(__dirname, 'src/server.tsx'),
  },
  module: {
    rules: [js],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name]',
  },
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    new webpack.HashedModuleIdsPlugin(),
  ],
  optimization: {
    //https://github.com/terser-js/terser
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: false, // Must be set to true if using source-maps in production
        terserOptions: {
          // https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
        },
      }),
    ],
  },
};

const clientConfig = {
  mode: 'production',
  resolve: {
    extensions: ['.ts', '.tsx', '.jsx', '.js', '.json'],
  },
  target: 'web',
  entry: [path.join(__dirname, 'src/client.tsx')],
  module: {
    rules: [js],
  },
  output: {
    path: path.resolve(__dirname, 'dist/public'),
    filename: '[name].js',
  },
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    new webpack.HashedModuleIdsPlugin(),
  ],
  optimization: {
    //https://github.com/terser-js/terser
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: false, // Must be set to true if using source-maps in production
        terserOptions: {
          // https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
        },
      }),
    ],
  },
};

module.exports = [serverConfig, clientConfig];


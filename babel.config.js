const isProd = process.env.NODE_ENV === 'production';

module.exports = function(api) {
  api.cache(true)

  const presets = [
    [
      '@babel/env',
      {
        targets: {
          edge: '17',
          firefox: '60',
          chrome: '67',
          safari: '11.1',
        },
        useBuiltIns: 'usage',
        corejs: '3',
        modules: false,
      },
    ],
    '@babel/react',
    '@babel/typescript',
  ]
  // dynamic import using es6 import() support
  const plugins = [
    '@babel/plugin-syntax-dynamic-import',
    '@babel/proposal-class-properties',
    "babel-plugin-styled-components",
    // isProd ? null : 'react-hot-loader/babel',
    ["import", { "libraryName": "antd", "libraryDirectory": "es", "style": true }] // `style: true` for less
  ]

  return {
    plugins,
    presets,
  }
}

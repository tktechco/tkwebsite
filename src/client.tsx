/*
* Tom Kobyluch
* client.tsx
*
* TODO:
*  - Redux codexport const App: React.FunctionComponent<IProps> = (props: IProps): JSX.Elemente splitting for dynamic reducer loading; https://redux.js.org/recipes/code-splitting
*  - When saga is implemented, client will be using saga store
*
* Notes:
*  - Polyfill is currently setup in babel.config.js, so not needed at the entry point.
*  - if this entry point is used, then assume client rendered, and use history type of browser, passed to connected-router
*  - History created here is for client render browser
*  - Great post on combinedReducers initial state, and using undefined; https://stackoverflow.com/questions/33749759/read-stores-initial-state-in-redux-reducer
*
* Code:
*   import "@babel/polyfill";
*
* */

import * as React from 'react';
import ReactDom from 'react-dom';
import configureStore from './store/configStore';
import { App } from './App';
import { createBrowserHistory } from 'history';
import IStore from './store/IStore';

// Grab the state from a global variable injected into the server-generated HTML
const preloadedState = window.__PRELOADED_STATE__;

// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__;

// take whatever state it took from the window, then using ... syntax, make my own changes
const clientInitialState: IStore = {
  ...preloadedState,
  RenderReducer: {
    ...preloadedState.RenderReducer,
    isServerSide: false,
  },
};

const clientHistory = createBrowserHistory();
const store: any = configureStore(clientInitialState, clientHistory);

ReactDom.hydrate(
  <App
    store={store}
    history={clientHistory}/>,
  document.getElementById('root')
);
/*
* Tom Kobyluch
* App.tsx
*
* TODO:
*
* Notes:
*
* */

import React from 'react';
import Home from './components/Home';
import { History } from 'history';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';

interface IProps {
  store: any;
  history: History;
}
export const App: React.FunctionComponent<IProps> = (props: IProps): JSX.Element => {
  // Main Grommet theme can be used for many controls
  return (
    <Provider store={props.store}>
        <ConnectedRouter history={props.history}>
          <Switch>
            <Route path="/" exact component={Home}/>
            <Route render={() => (<div>Miss</div>)} />
          </Switch>
        </ConnectedRouter>
    </Provider>
  );
};

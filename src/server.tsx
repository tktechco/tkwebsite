/*
* Tom Kobyluch
* webpack.config.js
*
*
* !! Currently disabling ssr to test express api proxy
*
* TODO:
*  - Clean up these notes
*  - Figure out how to only do ssr setup when necessary
*  - async eventually
*  - sagastore vs store ssr
*  - make typscript file with typing
*  - Write a file to create a store based on whether store creation is server side, instead of makeing store in server.js, and client.tsx
*  - change initialstate to partial
*  - initial state should have something to do with restoring page from history url
*  - Figure out if direct api links should have ssr
*  - make sure try catch styled components
*  - dom streaming
*
*
*
* Notes:
*  - So, if user enters website route /, the ssr will kick in for faster load of main page
*     - The store should be tracking history, and user should go back to where they were?
*     - If so, does that page also go through ssr race? Assuming the client does not have cached js already
*  - Important: store and history created here are only for ssr, client gets it when entering client.tsx
*  - Memory history is for ssr only
*  - express.static is important as ssr server is compiled without app js, it needs access to scripts
*  - styled components needs help for ssr : https://www.styled-components.com/docs/advanced#server-side-rendering
*  - style tags on server side render are shoved in head, maybe use helmet too?
*  - the html template is normal, it includes your js files as script src's, the compiled js that is now on client trys to load with react hydrate, if ReactDomServer rendered the home component faster, hydrate will no to stop and let srr win
*  - SSR render app directly, skipping client entrypoint, and make dom text
*/


// !!! important
// https://github.com/reduxjs/redux/blob/master/docs/recipes/ServerRendering.md

import { renderToString } from 'react-dom/server';
import { App } from './App';
import configureStore from './store/configStore';
import { createMemoryHistory } from 'history';
import { ServerStyleSheet, StyleSheetManager } from 'styled-components';
import IRootReducerState from './store/IStore';
import express from 'express';
import React from 'react';
import path from 'path';
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');

const app = express();
const config = require('../webpack/webpack.config.js');
const compiler = webpack(config);
app.use(webpackDevMiddleware(compiler, {
  publicPath: '/',
}));
app.use(require("webpack-hot-middleware")(compiler));

function handleRenderer(req, res) {

    const ssrInitialState: Partial<IRootReducerState> = {
      RenderReducer: { isServerSide: true },
    };

    const ssrHistory = createMemoryHistory();
    const ssrStore = configureStore(ssrInitialState as IRootReducerState, ssrHistory);

    // styled-components ssr jss
    // So is there any client side CSS? or is it all server side built.
    const sheet = new ServerStyleSheet();
    const component = (
      <StyleSheetManager sheet={sheet.instance}>
        <App store={ssrStore} history={ssrHistory}/>
      </StyleSheetManager>
    );

    const comm = renderToString(component);
  const preloadedState = ssrStore.getState();    // gettign state after the rendertostring could be better

  const styleTags = sheet.getStyleTags(); // or sheet.getStyleElement();
    sheet.seal();

    const htmltemplate = `
  <!doctype html>
    <html>
    <head>
      ${styleTags}
      <title>tktech</title>
    </head>
    <body>
    <div id="root">${comm}</div>
            <script>
          // WARNING: See the following for security issues around embedding JSON in HTML:
          // http://redux.js.org/recipes/ServerRendering.html#security-considerations
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(
      /</g,
      '\\u003c'
    )}
        </script>
    <script src="/public/main.js"></script>
  </body>
  </html>`;

    return htmltemplate;
}

app.use(express.static(path.resolve(__dirname, '..', 'dist')));
app.get('/', (req, res) => {
  const htmltemplate = handleRenderer(req ,res);
  res.send(htmltemplate);
  // res.sendFile(path.join(__dirname, 'dist/index.html')),
});
console.log('Started');
app.listen(8080);

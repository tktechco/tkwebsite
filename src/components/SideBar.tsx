/*
* Tom Kobyluch
* SideBar.tsx
*
* TODO:
*  - Stop subscribers all the components to the store with connect
*  - component will mount stuff
* Notes:
*
*
* Code:
*  - const PlayerWrapper = styled(ReactPlayer)``;
*
* */

import React from 'react';
import { connect } from 'react-redux';
import IStore from '../store/IStore';
import { Dispatch } from 'redux';
import { TOGGLE_SIDEBAR, VisibilityActionTypes } from '../store/visibility/IVisibilityAction';
import { Icon, Layout, Menu } from 'antd';
// import { TOGGLE_SIDEBAR } from '../store/visibility/IVisibilityAction';

const { Header, Sider, Content } = Layout;

/* Interfaces for interacting with the store*/
interface IProps {
  size: string;
}

interface IStateToProps {
  showSidebar: boolean;
}

interface IDispatchToProps {
  // dispatch?: (action: PlayerActionTypes | VisibilityActionTypes) => void;
  dispatch?: (action: VisibilityActionTypes) => void;
}

const mapDispatchToProps = (dispatch: Dispatch<any>): IDispatchToProps => ({
  dispatch,
});

const mapStateToProps = (state: IStore): IStateToProps => ({
  showSidebar: state.VisibilityReducer.showSidebar,
});

/* Main SideBar class */
class SideBar extends React.Component<IProps & IStateToProps & IDispatchToProps> {
  render() {
    const { showSidebar, dispatch } = this.props;

    const click = () => {
      dispatch({ type: TOGGLE_SIDEBAR });
    };

    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={showSidebar}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1">
              <Icon type="user" />
              <span>nav 1</span>
            </Menu.Item>
            <Menu.Item key="2">
              <Icon type="video-camera" />
              <span>nav 2</span>
            </Menu.Item>
            <Menu.Item key="3">
              <Icon type="upload" />
              <span>nav 3</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              className="trigger"
              type={showSidebar ? 'menu-unfold' : 'menu-fold'}
              onClick={click}
            />
          </Header>
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              background: '#fff',
              minHeight: 280,
            }}
          >
            Content
          </Content>
        </Layout>
      </Layout>
    );
  }
}
export default connect<IStateToProps>(mapStateToProps, mapDispatchToProps)(SideBar);
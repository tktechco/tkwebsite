import React, { Component } from 'react';
import { connect } from 'react-redux';
import IStore from '../store/IStore';
import { Grommet, Box, Grid, Heading, ResponsiveContext } from 'grommet';
import { grommet } from 'grommet/themes';
import { deepMerge } from 'grommet/utils';

interface IStateToProps {
  count?: number;
  showSidebar: boolean;
}

const mapStateToProps = (state: IStore): IStateToProps => ({
  // count: store.counterReducerState.count,
  count: state.CounterReducer.count,
  showSidebar: state.VisibilityReducer.showSidebar,
});

class Posts extends Component<IStateToProps> {


  render() {

    return (
      <ResponsiveGrid/>
    );
  }
}



const animals = [
  'dog',
  'cat',
  'pig',
  'cow',
  'giraffe',
  'elephant',
  'dinosaur',
  'chicken',
  'duck',
  'tiger',
  'lion',
  'cheetah',
];

// map api input to boxes
const listAnimalsBoxes = animals.map(animalName => (
  <Box
    elevation="large"
    key={animalName}
    background="light-3"
    flex={true}
    justify="center"
    align="center"
  >
    <Heading level={2}>{animalName}</Heading>
  </Box>
));



const ResponsiveGrid = () => (

    <Responsive gap="small" margin="medium" columns="medium" rows="xsmall">
      {listAnimalsBoxes}
    </Responsive>
  </Grommet>
);


export default connect<IStateToProps>(mapStateToProps)(Posts);
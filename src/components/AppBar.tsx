/*
* Tom Kobyluch
* AppBar.tsx
*
* TODO:
*  - Give actions on reducers a typing
*
* Notes:
*
* */

import React from 'react';
import { connect } from 'react-redux';
import IStore from '../store/IStore';
import { Box, Button, Heading } from 'grommet';
import { Dispatch } from 'redux';
import { TOGGLE_SIDEBAR } from '../store/visibility/IVisibilityAction';
import { Menu, Github } from 'grommet-icons';

interface IStateToProps {
  showSidebar: boolean;
}

interface IDispatchToProps {
  dispatch?: (action: any) => void;
}

const mapDispatchToProps = (dispatch: Dispatch<any>): IDispatchToProps => ({
  dispatch,
});

const mapStateToProps = (state: IStore): IStateToProps => ({
  showSidebar: state.VisibilityReducer.showSidebar,
});

const AppBarBox = (props: any) => (
  <Box
    tag="header"
    direction="row"
    align="center"
    justify="between"
    background="brand"
    pad={{ left: 'medium', right: 'small', vertical: 'small' }}
    elevation="medium"
    style={{ zIndex: '1' }}
    {...props}
  />
);

class Home extends React.Component<IStateToProps & IDispatchToProps> {

  render() {

    // TODO: Things like componentWillMount, etc.
    const { dispatch } = this.props;

    const click = () => {
      dispatch({ type: TOGGLE_SIDEBAR });
    };

    return (
      <AppBarBox>
        <Heading level='3' margin='none'>tktech</Heading>
        <Button icon={<Github/>} href={'https://bitbucket.org/tktechco/tkwebsite/src/master/'}/>
        <Button icon={<Menu/>} onClick={click} color={'accent-1'}/>
      </AppBarBox>
    );
  }
}

export default connect<IStateToProps>(mapStateToProps, mapDispatchToProps)(Home);
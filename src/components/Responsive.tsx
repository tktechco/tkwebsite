// Thomas Kobyluch
// Responsive.tsx
// A component wrapper for making it responsive via grommet

import { Grid, Grommet, grommet, ResponsiveContext } from 'grommet';
import React from 'react';
import { deepMerge } from 'grommet/utils';

const Responsive = ({ children: children, areas: areas, ...props }) => {
  return (
    // The idea here is to simple merge new breakpoints into my main theme, seen in App.tsx
    <Grommet theme={customBreakpoints}>
      <ResponsiveContext.Consumer>
        {size => {
          // Take into consideration if not array is sent but a simple string
          let columnsVal = columns;
          if (columns) {
            if (columns[size]) {
              columnsVal = columns[size];
            }
          }

          let rowsVal = rows;
          if (rows) {
            if (rows[size]) {
              rowsVal = rows[size];
            }
          }

          // Also if areas is a simple array not an object of arrays for different sizes
          let areasVal = areas;
          if (areas && !Array.isArray(areas)) {
            areasVal = areas[size];
          }

          return (
            <Grid
              {...props}
              areas={!areasVal ? undefined : areasVal}
              rows={!rowsVal ? size : rowsVal}
              columns={!columnsVal ? size : columnsVal}
            >
              {children}
            </Grid>
          );
        }}
      </ResponsiveContext.Consumer>
    </Grommet>
  );
};

const customBreakpoints = deepMerge(grommet, {
  global: {
    breakpoints: {
      small: {
        value: 250,
      },
      medium: {
        value: 2500,
      },
      large: 5000,
    },
  },
});

// The amount of columns or rows based on breakpoints
const columns = {
  small: ['auto'],
  medium: ['auto', 'auto'],
  large: ['auto', 'auto', 'auto'],
  xlarge: ['auto', 'auto', 'auto'],
};

const rows = {
  small: ['xsmall', 'xsmall', 'xsmall'],
  medium: ['xsmall', 'xsmall'],
  large: ['xsmall'],
  xlarge: ['xsmall'],
};
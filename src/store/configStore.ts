/*
* Tom Kobyluch
* configStore.ts : Setup middleware and export store, which is made from all the reducers combined
*
* TODO:
*  - Use a logger middleware in devEnhancer
*  - Setup tying on whole configStore function
*  - consider how saga is run client vs server side
*  - Implement initial state from client window
*
* Notes:
*  - typescript provides partial interfaces, allowing one to use an interface without filling out whole interface
*  - with conditionals use type guards; https://basarat.gitbooks.io/typescript/docs/types/typeGuard.html
*  - A store enhancer is a higher-order function that composes a store creator to return a new, enhanced store creator.
*  - This is similar to middleware in that it allows you to alter the store interface in a composable way.
*  - The store must be given the history, so the router reducer can access it
*  - History should not be typed as History, for now, as we have two types of histories
*  - Because a store is not an instance, but rather a plain-object collection of functions, copies can be easily created and modified without mutating the original store.
*  - Remember, say you want to add middleware to devEnhancer, you use the spread operator and provide new middleware, like object assign, but es6
*
* For Saga Implementation:
*   Notes:
*     - Look for development logging for saga
*
*   Code:
*     const sagaMiddleware: SagaMiddleware<any> = createSagaMiddleware();
*     sagaMiddleware.run(root);
*     store.close = () => store.dispatch(END)
*     const action = (type) => store.dispatch({type});
*
* */

import { createStore, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import rootReducer from './rootReducer';
import { routerMiddleware } from 'connected-react-router';
import IStore from './IStore';

const isProd = process.env.NODE_ENV === 'production';

export default function configureStore(initialState: IStore, history: any) {

  const middlewares = [
    routerMiddleware(history),
  ];

  const devEnhancer = composeWithDevTools(
    applyMiddleware(...middlewares)
  );
  const prodEnhancer = compose(
    applyMiddleware(...middlewares)
  );

  const enhancer = isProd ? prodEnhancer : devEnhancer ;

  const store: any = createStore(
    rootReducer(history),
    initialState,
    enhancer
  );

  return store;
}

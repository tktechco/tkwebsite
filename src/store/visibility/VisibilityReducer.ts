/*
* Tom Kobyluch
* webpack.config.js
*
* TODO:
*
* Notes:
*
* */

import IVisibilityReducerState from './IVisibilityReducerState';
import { VisibilityActionTypes, TOGGLE_SIDEBAR } from './IVisibilityAction';

// CounterAction.ts , ICounterAction.ts , and ICounterReducerState.ts can ultimetly be ignored as they exist
// to make this reducer happen
// calling methods on class is easier to understand

const initialState: IVisibilityReducerState = {
  showSidebar: true,
};

// public as it will be called on class later. remember static as this is not instanced
// we can return the actual reducer object Reducer<ICounterReducerState>, helping out rootReducer
export default function VisibilityReducer(state: IVisibilityReducerState = initialState, action: VisibilityActionTypes): IVisibilityReducerState {
  switch (action.type) {
    // toggle side bar
    case TOGGLE_SIDEBAR:
      return {
        ...state,
        showSidebar: !state.showSidebar,
      };
    default:
      return state;
  }
}


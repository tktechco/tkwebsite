import { VisibilityActionTypes } from './IVisibilityAction';

// TODO: should define what an action is, and check against that

/*export function updateSession(state: ICounterState): CounterTypes {
  return {
    type: UPDATE_COUNTER,
    payload: state,
  };
}*/

// when to use readonly? whenever type definition / interface member should be immutable
export default class VisibilityAction {

  // todo type check by defining what action is below
  public static toggleSidebar(): VisibilityActionTypes {
    return {
      type: 'TOGGLE_SIDEBAR',
    };
  }
}
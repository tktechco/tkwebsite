import { Action } from 'redux';

export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR'

interface IVisibilityToggleSidebarAction extends Action{
  type: typeof TOGGLE_SIDEBAR;
}

export type VisibilityActionTypes = IVisibilityToggleSidebarAction; // | IUpdateSessionAction;
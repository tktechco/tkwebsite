/*
* Tom Kobyluch
* webpack.config.js
*
* TODO:
*
* Notes:
*  - I should not need any actions for RenderReducer as we are only checking boolean
*
* */

import IRenderReducerState from './IRenderReducerState';

const initialRenderState: IRenderReducerState = {
  isServerSide: true,
};

export default function RenderReducer(state: IRenderReducerState = initialRenderState, action: IRenderReducerState): IRenderReducerState {
      return state;
}


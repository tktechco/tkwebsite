/*
* Tom Kobyluch
* webpack.config.js
*
* TODO:
*  - seperate connected router reducer directory
*  - give connected router typing in store
*  - consider whether returning anonymous function is preformance impact
*  - give typing to the rootReducer, it is related to IStore
*  - check typing on history
*
* Notes:
*  - rootReducer is redux Reducer of type interface IStore, IStore being a combination of states and their types
*  - It seems connected router really wants the name to be "router"
*
* Code:
*
* */

import CounterReducer from './counter/CounterReducer';
import { combineReducers } from 'redux';
import VisibilityReducer from './visibility/VisibilityReducer';
import { connectRouter } from 'connected-react-router';
import RenderReducer from './render/RenderReducer';

export default (history: any) => combineReducers({
  router: connectRouter(history),
  CounterReducer,
  VisibilityReducer,
  RenderReducer,
});
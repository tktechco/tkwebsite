/*
* Tom Kobyluch
* IRenderReducerState.ts : Typing for RenderReducer
*
* TODO:
*
* Notes:
*  - the store is made of reducers. these reducers act on components with their own states. so states also make up store
*  - An interface is provided to restrict the store to a known quantity, and for typing
*  - State is what is checked against this
*  - Your store is basically your reducers. Reducers have states. So logically,
* */

import ICounterReducerState from './counter/ICounterReducerState';
import IVisibilityReducerState from './visibility/IVisibilityReducerState';
import IRenderReducerState from './render/IRenderReducerState';

export default interface IRootReducerState {
  CounterReducer: ICounterReducerState;
  VisibilityReducer: IVisibilityReducerState;
  router: any;
  RenderReducer: IRenderReducerState;
}


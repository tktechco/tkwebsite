import { Action } from 'redux';

export const UPDATE_COUNTER = 'CounterAction.UPDATE_COUNTER'
export const INCREMENT_COUNTER = 'CounterAction.INCREMENT_COUNTER'
export const DECREMENT_COUNTER = 'CounterAction.DECREMENT_COUNTER'

interface IUpdateCounterAction extends Action{
  type: typeof UPDATE_COUNTER;
  payload: number;
}
/*interface DeleteMessageAction {
  type: typeof DELETE_MESSAGE
  meta: {
    timestamp: number
  }
}*/

export type CounterActionTypes = IUpdateCounterAction; // | IUpdateSessionAction;
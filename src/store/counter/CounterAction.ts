import { CounterActionTypes, UPDATE_COUNTER } from './ICounterAction';

// TODO: should define what an action is, and check against that

/*export function updateSession(state: ICounterState): CounterTypes {
  return {
    type: UPDATE_COUNTER,
    payload: state,
  };
}*/

// when to use readonly? whenever type definition / interface member should be immutable
export default class CounterAction {

  // todo type check by defining what action is below
  public static updateCounterAction(inc: number): CounterActionTypes {

    return {
      type: UPDATE_COUNTER,
      payload: inc,
    };
  }
}
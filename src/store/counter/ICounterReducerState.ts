
// like the store interface, restrict the state changes to this typing

export default interface ICounterReducerState {
  readonly count: number;
}
/*
* Tom Kobyluch
* CounterReducer.ts
*
* TODO:
*  - Clean up notes

* Notes:
// CounterAction.ts , ICounterAction.ts , and ICounterReducerState.ts can ultimetly be ignored as they exist
    // to make this reducer happen
// calling methods on class is easier to understand
*
*   // public as it will be called on class later. remember static as this is not instanced
  // we can return the actual reducer object Reducer<ICounterReducerState>, helping out rootReducer
*
*
* Code:
*/

import ICounterReducerState from './ICounterReducerState';
import { CounterActionTypes, UPDATE_COUNTER } from './ICounterAction';

const initialState: ICounterReducerState = {
  count: 5,
};


export default function CounterReducer(state: ICounterReducerState = initialState, action: CounterActionTypes): ICounterReducerState {
  switch (action.type) {
    case UPDATE_COUNTER:
      return {
        ...state,
        count: action.payload,
      };
    default:
      return state;
  }
}


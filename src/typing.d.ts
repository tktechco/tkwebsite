// for my custom typing declarations.
// Created for window global. typescript gives error.
// Window used in ssr   https://github.com/reduxjs/redux/blob/master/docs/recipes/ServerRendering.md

interface Window {
  __PRELOADED_STATE__?: any;
}

declare namespace NodeJS {
  interface Global {
    window: Window;
  }
}